package main

import (
	"github.com/jacobsa/go-serial/serial"
	"fmt"
	zmq "github.com/pebbe/zmq4"
	"github.com/vmihailenco/msgpack"
	"github.com/mrVanboy/go-simple-cobs"
	"encoding/binary"
	"os"
	"time"
	"log"
	"github.com/go-ini/ini"
	"io"
	"bytes"
	"io/ioutil"
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"strings"
)

var version = "0.14"

var sendToSerialDelay = 5
var pubBind, repBind string

type CanMsg struct {
	Event     uint16
	DataBit   uint8
	Address   uint16
	Data      []byte
	Timestamp int64
}

var port io.ReadWriteCloser

var cfg *ini.File

var chanMysqlFull bool

func WriteSqlLog(chanMysql chan CanMsg) {
	errMysqlDisconnect := false

	db, err := sql.Open("mysql", "csh:csh@tcp(localhost:3306)/log")
	if err != nil {
		panic(err.Error()) // Just for example purpose. You should use proper error handling instead of panic
	}
	defer db.Close()

	for {
		msg2sql := <-chanMysql // слушаем канал chanMysql

		sqlValues := []string{"NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL", "NULL"}

		for i, v := range msg2sql.Data {
			sqlValues[i] = fmt.Sprint(v)
		}

		SqlQuery := "INSERT INTO log VALUES (?, ?, ?, ?," + strings.Join(sqlValues, ",") + ")"

	Reinsert:
		_, err := db.Exec(
			SqlQuery,
			msg2sql.Timestamp, msg2sql.Event, msg2sql.DataBit, msg2sql.Address)
		if err != nil {

			if strings.Contains(err.Error(), "connect: connection refused") ||
				strings.Contains(err.Error(), "commands out of sync") {
				if errMysqlDisconnect == false {
					log.Println("MySQL disconnect")
					errMysqlDisconnect = true
				}
				time.Sleep(time.Second) // reconnect to MySQL server pause
				goto Reinsert
			} else {
				log.Println("MySQL errror: ", err)
			}
		} else {
			if errMysqlDisconnect == true {
				log.Println("Connect MySQL")
			}
			errMysqlDisconnect = false
		}

	}

}

func CanMsg2Array(msg CanMsg) []uint16 {
	MsgArray := []uint16{msg.Event, uint16(msg.DataBit), msg.Address}
	for i := range msg.Data {
		MsgArray = append(MsgArray, uint16(msg.Data[i]))
	}
	return MsgArray
}

/******* ZMQServers *******************/

func PubServer(chanSerialReceive chan CanMsg) {

	type CanMsgPub struct {
		Timestamp int64
		Msg       []uint16
	}

	var buf bytes.Buffer
	var msg2pub CanMsgPub
	enc := msgpack.NewEncoder(&buf).StructAsArray(true)

	//  Socket to talk to clients
	pub, _ := zmq.NewSocket(zmq.PUB)
	defer pub.Close()
	pub.Bind(pubBind)

	for {
		msg := <-chanSerialReceive // слушаем канал chanSerialReceive
		msg2pub.Timestamp = msg.Timestamp
		msg2pub.Msg = CanMsg2Array(msg)
		enc.Encode(msg2pub)
		readBuf, _ := ioutil.ReadAll(&buf)
		pub.Send(fmt.Sprintf("EV%05d", msg.Event)+string(readBuf[:]), 0)
	}
}

func RepServer(chanSerialReceive chan CanMsg, chanSerialWrite chan CanMsg) {
	//  Socket to talk to clients
	responder, _ := zmq.NewSocket(zmq.REP)
	defer responder.Close()
	responder.Bind(repBind)
	var decodedArray []uint16

	for {
		//  Wait for next request from client
		msg, _ := responder.Recv(0)

		if msg[:1] == "\x00" { // "send can" сообщение
			err := msgpack.Unmarshal([]byte(msg[1:]), &decodedArray)
			if err != nil {
				responder.Send("ERROR", 0)
			} else {
				if len(decodedArray) >= 3 && len(decodedArray) <= 13 { // send can message
					var can CanMsg
					can.Event = decodedArray[0]
					can.DataBit = uint8(decodedArray[1])
					can.Address = decodedArray[2]
					for _, val := range decodedArray[3:] {
						can.Data = append(can.Data, uint8(val))
					}
					can.Timestamp = time.Now().UnixNano()
					// отправляем в канал
					chanSerialReceive <- can
					chanSerialWrite <- can
					time.Sleep(time.Duration(sendToSerialDelay) * time.Millisecond)
					//  Send reply back to client
					responder.Send("OK", 0)
				} else {
					responder.Send("ERROR", 0)
				}
			}

		} else {
			responder.Send("ERROR", 0)
		}

	}
}

/******* Serial ***********************/

func writeSerial(chanSerialWrite chan CanMsg) {
	var eid uint32 = 0

	for {
		msg := <-chanSerialWrite                              // слушаем канал chanSerialWrite
		eid = uint32(msg.Event)<<13 + uint32(msg.Address) // преобразовываем в EID
		if msg.DataBit > 0 {
			eid |= 4096
		}

		// create byte from uint32
		writeByte := make([]byte, 4)
		binary.BigEndian.PutUint32(writeByte, eid)
		for i := 0; i < len(msg.Data); i++ {
			writeByte = append(writeByte, msg.Data[i])
		}

		encoded, err := cobs.Encode(writeByte)
		if err != nil {
			log.Println("error encode cobs")
		}
		encoded = append(encoded, 0)

		port.Write(encoded)

	}
}

func readSerial(chanSerialReceive chan CanMsg, chanMysql chan CanMsg) {
	buf := make([]byte, 20)
	var msg CanMsg
	var n int
	for {
		for i := range buf {
			buf[i] = 0
		}

		n = 0
		n, _ = port.Read(buf)

		decoded, err := cobs.Decode(buf[:(n - 1)])
		if err != nil {
			log.Println("error cobs")
		} else {
			if (len(decoded) >= 4) && (len(decoded) <= 12) {
				// normal packet
				var eid = binary.BigEndian.Uint32(decoded[:4])
				msg.Event = uint16(eid >> 13)
				msg.DataBit = uint8(eid >> 12 & 1)
				msg.Address = uint16(eid & 0xFFF)
				msg.Data = decoded[4:]
				msg.Timestamp = time.Now().UnixNano()
				// отправляем в канал
				chanSerialReceive <- msg
				// отправляем в очередь
				if len(chanMysql) >= 99 {
					if chanMysqlFull == false {
						log.Println("buffer chanMysql full")
					}
					chanMysqlFull = true

				} else {
					chanMysql <- msg
					chanMysqlFull = false
				}


			}
		}
	}
}

func main() {
	if len(os.Args) == 2 {
		if os.Args[1] == "-version" {
			fmt.Println(version)
			os.Exit(0)
		}
	}

	chanSerialReceive := make(chan CanMsg, 10)
	chanSerialWrite := make(chan CanMsg, 10)
	chanMysql := make(chan CanMsg, 100)

	// load ini file
	var err error
	cfg, err = ini.Load("./csh_loggerd.ini")
	if err != nil {
		log.Fatalf("Fail to read file: %v", err)
	}

	// INI
	sendToSerialDelay = cfg.Section("").Key("send_to_serial_delay").MustInt()

	var options = serial.OpenOptions{
		PortName:        cfg.Section("serial").Key("port_name").MustString("/dev/ttyS0"),
		BaudRate:        cfg.Section("serial").Key("baud_rate").MustUint(),
		DataBits:        8,
		StopBits:        1,
		MinimumReadSize: 4,
	}

	pubBind = cfg.Section("zmq").Key("pub_bind").MustString("tcp://*:5432")
	repBind = cfg.Section("zmq").Key("rep_bind").MustString("tcp://*:5560")

	port, err = serial.Open(options)

	if err != nil {
		log.Fatal("Serial port not open: ", options.PortName)
	}
	defer port.Close()

	//go ConnectSQL()

	go readSerial(chanSerialReceive, chanMysql)

	go PubServer(chanSerialReceive)

	go RepServer(chanSerialReceive, chanSerialWrite)

	go writeSerial(chanSerialWrite)

	go WriteSqlLog(chanMysql)

	for {
		//writeSerial()
		time.Sleep(1000 * time.Millisecond)
	}

}
